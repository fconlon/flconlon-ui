import { useState } from "react"
import { Link,
  Typography,
  Button,
  Collapse
} from "@mui/material"
import { ExpandMore, ExpandLess } from "@mui/icons-material"

const TableOfContents = () => {
  const [contentsOpen, setContentsOpen] = useState(false)
  return (
    <>
      <Button 
        sx={{ color: 'black' }}
        startIcon={contentsOpen ? <ExpandLess /> : <ExpandMore />}
        endIcon={contentsOpen ? <ExpandLess /> : <ExpandMore />}
        onClick={() => setContentsOpen(!contentsOpen)}
      >
        Table of Contents
      </Button>
      <br />
      <Collapse in={contentsOpen}>
        <Typography component='span' sx={{textIndent: '0px', marginTop: '20px'}}>
          <Link href='#aboutsite' underline='hover'>About This Site</Link>
          <br />
          <Link href='#whoami' underline='hover'>Whoami</Link>
          <br />
          <Link href='#experience' underline='hover'>
              Profesional Experience
          </Link>
          <br />
          <Link href='#demos' underline='hover'>Demos</Link>
        </Typography>
      </Collapse>
    </>
    
  )
}

export default TableOfContents