import { Box, Typography, Link } from "@mui/material"
import { downloadResume } from "../../../common/helpers"
import TableOfContents from "./table_of_contents"

const QuickInfo = () => {
  return (
    <Box sx={{ textAlign: 'center'}}>
      <Typography variant='h3'>
        Frank Conlon
      </Typography>
      <Typography sx={{ textIndent: '0px'}}>
        Full Stack Software Engineer, check out my resume below!
        <br />
        <Link component='button' onClick={downloadResume}>
          Resume
        </Link>
      </Typography>
      <TableOfContents />
    </Box>
  )
}

export default QuickInfo