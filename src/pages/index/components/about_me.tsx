import { Typography, Divider } from "@mui/material"

const AboutMe = () => {
  return (
    <>
      <Divider textAlign='left' variant='middle'>
        <Typography variant='h4' id='whoami'>Whoami (Linux humor ftw 😋)</Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        Hello, my name is Frank Conlon. I am a Full stack Software Engineer 
        with experience in a variety of tech stacks. Most recently I have been
        working with React/Django, but I also have experience with J2EE, 
        Springboot and Flask. I am familiar with and proficient at coding in a 
        variety of languages including C/C++, Java, Python, Javascript, and 
        Typescript. My biggest strength though is my ability to research
        problems and develop effective solutions with little or no guidance. 
        I'm not necessarily the guy that already knows the solution, but I 
        am always able to find or create the solution.
      </Typography>
      <Typography sx={{padding: '20px'}}>
        At home I am a father to three children and husband to my wife Chrystal. My youngest
        is seven years old and my oldest two are 14. I'm fortunate to have not one, but TWO
        teenagers who still know everthing 😂. In my free time I like to play video games 
        and watch various tv shows with my wife. She has a bit of a soft spot for reality TV
        and true crime stories. We have three dogs, Nacho, Jiraya and Raider, who like to beg
        for pets and steal food 😋. We love them to death though and wouldn't trade them for 
        anything. 
      </Typography>
      <Divider textAlign='left' variant='middle'>
        <Typography variant='h4' id='experience'>Professional Experience</Typography>
      </Divider>
      <Divider textAlign='left' variant='inset' sx={{ marginTop: '20px'}}>
        <Typography variant='h5' id='teksystems'>TEKSystems</Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        At TEKSystems I served as a consultant for Bank of America in a React/Django tech 
        stack with an Oracle database for persisting data. I primarily worked on the backend 
        apis, but I also code reviewed all front end code and helped to make architectural
        decisions to improve performance. I was heavily involed in requirements elicitation
        and assisted in the desgin of the dataflow for both the front and backend 
        applications. I redesigned the APIs and front end calls to eliminate timeouts that
        were occuring due to poor data retrieval design. I also assisted with database
        design decisions that were instrumental in ensuring that customer expectations were
        met. 
      </Typography>
      <Divider textAlign='left' variant='inset'>
        <Typography variant='h5' id='briebug'>Briebug</Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        At Briebug I served as a consultant for FedEx in an Angular/Springboot tech stack
        with an Oracle database for persisting data. When I first started working there
        I was handed 15 interdepent repositories for an application and told, "The guy who
        wrote these isn't avaialbe anymore and we don't know how they work. Have fun." 😂.
        I had to reverse engineer the code to figure out how it worked so I could improve
        and maintain it for the company. Aside from that, FedEx also did not have 
        well developed agile processes on the team that I was assigned to, so I helped to
        implement new SAFe (they were already using SAFe, just not very well) agile 
        processes to improve the developer experience.
      </Typography>
      <Divider textAlign='left' variant='inset'>
        <Typography variant='h5' id='usaa'>USAA</Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        At USAA I worked as a full stack developer in a tech stack that used React/Redux 
        and J2EE with an IBM DB2 mainframe for out database. We mostly use RESTful apis
        for our backend, but we did have a few legacy APIs that used SOAP. Here we used
        Elastic Search and splunk for parsing logs, and I had to set up automated alerts
        from Elastic Search to let us know when our endpoints were failing. We used the
        Scaled Agile Framework for enterprises (SAFe) methodology for all of our agile 
        procedures.
      </Typography>
      <Divider textAlign='left' variant='inset'>
        <Typography variant='h5' id='texastech'>Texas Tech University</Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        I fulfilled several roles in my time at Texas Tech including Tutor, TA,
        Graduate Assistant, IT Support, and Full Time Instructor.
      </Typography>
      <Typography variant='h6' sx={{ marginLeft: '60px'}}>
        Instructor
      </Typography>
      <Typography sx={{padding: '20px'}}>
        As an instructor I taught between 3 and 4 classes each semester with anyhere
        from 250 to 350 students. I taught virtually every CS class under the sun. If
        none of the tenured professors wanted to teach a class I was the one that 
        ended up teaching it. As a result I taught Programming Principles I/II (python
        and c respectively), Data Structures, Assembly Language, Computer Architecture,
        Software Engineering, and Logic Programming. I was responsible for developing
        the curriculum, tests, and assigments as well as grading everything that was
        assigned.
      </Typography>
      <Typography variant='h6' sx={{ marginLeft: '60px'}}>
        Teaching Assistant
      </Typography>
      <Typography sx={{padding: '20px'}}>
        As a teaching assistant I was essentially a full time instructor for a Python
        lab. I had to create assignments and lesson plans each week and grade them,
        as well as provided individual and group instruction on Python Programming
        Principles during each weekly lab.
      </Typography>
      <Typography variant='h6' sx={{ marginLeft: '60px'}}>
        Graduate Assistant
      </Typography>
      <Typography sx={{padding: '20px'}}>
        As a graduate assistant my primary responsibility was to resarch ways to 
        better parallelize the pmap utility that genetics researchers use to 
        perform string matching on massive (more than 3 billion charaters for
        the human genome) strings. The pmap utility used a shared memory model
        (threads) and we were trying to parallelize it for a distrubuted memory
        model.
      </Typography>
      <Typography variant='h6' sx={{ marginLeft: '60px'}}>
        IT Support
      </Typography>
      <Typography sx={{padding: '20px'}}>
        As an IT support specialist I provided IT support for anyone who used any
        of the super computers at the TTU High Performance Computing Center. I also
        helped to maintain the TTUHPC website and participated in the Construction
        of the Quanah super computer.
      </Typography>
      <Typography variant='h6' sx={{ marginLeft: '60px'}}>Tutor</Typography>
      <Typography sx={{padding: '20px'}}>
        As a tutor I worked in the Learning Center where students could come for
        free tutoring in any subject the university offered. I personally tutored
        math classes from basic Algebra up to Calculus 3, Linear Algebra, Statistics,
        all Computer Science courses and Matlab. I was the only Matlab tutor the 
        Learning Center had. Even though I never had any formal training in Matlab,
        I was able to pick it up on the fly and tutor it for several different 
        engineering courses, including a graduate engineering course while I was
        an undergrad.
      </Typography>
      <Divider textAlign='left' variant='inset'>
        <Typography variant='h5' id='internships'>Internships</Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        I interned for two summers at Texas Intstruments as an undergraduate student,
        and for two more summers at Sandia National Labs as a graduate student. Both
        internships provided me with unique perspectives and challenges.
      </Typography>
      <Typography variant='h6' sx={{ marginLeft: '60px'}}>
        Sandia National Labs
      </Typography>
      <Typography sx={{padding: '20px'}}>
        I spent two summers at SNL working in their Center for Cyber defenders program.
        My first summer I worked on a project to test the feasibility of using bluetooth
        to transmit messages across a city in the event of a natural disaster that 
        knocked out cell phone towers. In my second summer I worked on a project where
        I used conditional probably to detect malicious instructions being sent to 
        Industrial Control Systems (the systems that operate infrastructure such as power
        grids and traffic lights). As a side procject I also wrote a small program for 
        an SPI board that was used to detect temperatures (I wasn't told what it was 
        measuring temperatures for).
      </Typography>
      <Typography variant='h6' sx={{ marginLeft: '60px'}}>
        Texas Instruments
      </Typography>
      <Typography sx={{padding: '20px'}}>
        At Texas Instruments I helped automate the processes that create the integrated
        circuits in their fabrication facilites. I had the opportunity to go inside
        two of their fabs and see how the processes work. The section that I worked in 
        at TI used a propietary language based on shell scripting that was adapted for
        the unique challenges of semiconductor automation. During my second summer I was
        given a manual for a brand new tool that they had just bought (which had never
        been automated anywhere in the world before) and completely automated it from 
        start to finish. In fairness, the tool was a fairly simple 'oven', but I still
        like to imagine the EE's after I left saying "The intern did it in 3 months.
        What's taking you guys so long?" 😋. 
      </Typography>
      <Divider textAlign='left' variant='inset'>
        <Typography variant='h5' id='army'>US Army</Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        The Army is where I learned how to persevere in the face of adversity and
        refined my leadership style. As an Ammunition Specialist I was responsible
        for the care, storage, and maintenance of all of the ammunition in an
        ammunition supply point. As a Sergeant I was responsible for the development
        and training of all of my subordinates. My experiences as a leader in the
        Army reinforced my leadership style. I found that the best way to lead, 
        even when you DO have the authority punish people, is still to make sure
        that your subordinates are taken care of. You take care of them and they
        will take care of you.
      </Typography>
    </>
  )
}

export default AboutMe