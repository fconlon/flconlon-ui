import { 
  useState,
  SyntheticEvent,
} from 'react'
import { Link as RRLink } from "react-router-dom"
import { Link, Box, Divider } from "@mui/material"
import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'

const DemosAccordion = () => {
  const [expanded, setExpanded] = useState<string | false>(false);

  const handleChange =
    (panel: string) => (_: SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  return (
    <>
      <Divider textAlign='left'>
        <Typography variant='h4' id='demos'>
          Demos (Under Construction)
        </Typography>
      </Divider>
      <Box sx={{padding: '20px'}}>
        <Accordion 
          expanded={expanded === 'panel1'}
          onChange={handleChange('panel1')}
          sx={{ marginTop : '15px' }}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography sx={{ width: '33%', flexShrink: 0 }}>Chore Tracker</Typography>
            <Typography sx={{ color: 'text.secondary' }}>Coming Soon</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              This app allows parents to track their childern's chores and homework,
              set rewards, and mark them complete. It also allows the children to see
              what chores they have to complete and what their rewards will be for 
              finishing them. It provides a calender for scheduling regular assignments
              or work and offers simple recomendations for tasks and rewards.
              <Box sx={{ textAlign: 'center' }}>
                <Link component={RRLink} to={'/choretracker'} underline='hover'>Chore Tracker</Link>
              </Box>
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2bh-content"
            id="panel2bh-header"
          >
            <Typography sx={{ width: '33%', flexShrink: 0 }}>
              Titanic Survivor Visualization
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              This app uses the Aframe javascript library to provide a visualization of
              the survivor data from the titanic maritime disaster. Clicking on any 
              cylinder will apply a filter to the data and adjust the heights of all of
              the cylinders in the visualization. Clicking the cylinder a second time 
              will remove the filter from the data. You can also move around using the 
              wasd keys and rotate the camera by clicking the viewscreen and moving the 
              mouse.
              <Box sx={{ textAlign: 'center' }}>
                <Link component={RRLink} to={'/titanic'} underline='hover'>Titanic</Link>
              </Box>
            </Typography>
          </AccordionDetails>
        </Accordion>
      </Box>
    </>
  );
}

export default DemosAccordion