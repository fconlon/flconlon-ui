import { Divider, Typography } from "@mui/material"

const AboutThisSite = () => {
  return (
    <>
      <Divider textAlign='left' >
        <Typography variant='h4' id='aboutsite'>
          About This Site
        </Typography>
      </Divider>
      <Typography sx={{padding: '20px'}}>
        I created this site as a place to practice my skills, test new technologies,
        and demonstrate my abilities. It is hosted with 
        <span style={{ fontWeight: 'bold' }}> AWS Amplify</span>. 
        It utilizes the <span style={{ fontWeight: 'bold' }}>React MaterialUI</span> library
        for the front end and <span style={{ fontWeight: 'bold' }}>Flask </span>
        for the chore tracker (coming soon) backend. For the Titanic visualization backend
        it uses an <span style={{ fontWeight: 'bold' }}>AWS Lambda</span> function that is triggered 
        by <span style={{ fontWeight: 'bold' }}>AWS API Gateway</span>. For persistent storage 
        it uses an <span style={{ fontWeight: 'bold' }}>AWS PostgreSQL</span> database. I manage 
        the <span style={{ fontWeight: 'bold' }}>DNS</span> myself through GoDaddy. Before migrating 
        to <span style={{ fontWeight: 'bold' }}>Amplify</span> the front end was hosted in
        an <span style={{ fontWeight: 'bold' }}>AWS EC2</span> instance with an
        <span style={{ fontWeight: 'bold' }}> Nginx</span> reverse proxy server to serve static 
        content. I previously had an email server set up in the same instance using
        <span style={{ fontWeight: 'bold' }}> Postfix, Dovecot and SOGo</span>. It was fun setting
        the email server up but, due to the cost of running an extra EC2 instance and the headache
        of self managing an email server, I decided to shut that instance down. 
      </Typography>
    </>
  )
}

export default AboutThisSite