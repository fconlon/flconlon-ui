import Carousel from 'react-material-ui-carousel'
import { ImageListItem, ImageListItemBar } from '@mui/material'
import family from '../../../assets/family_photo.jpg'
import couple from '../../../assets/couple_photo.jpg'
import headshot from '../../../assets/headshot.jpg'


const PicturesCarousel = () => {
  

  return (
    <Carousel sx={{ width: 421, margin: 'auto', marginTop: '20px'}} interval={6000}>
      <ImageListItem>
        <img 
          src={family} 
          style={{ width: 421, height: 527}} 
          alt="I picture of me with my family"
        />
        <ImageListItemBar title={'Family Fun Day'} />
      </ImageListItem>
      <ImageListItem sx={{ width: 351, margin: 'auto' }}>
        <img 
          src={couple} 
          style={{ width: 351, height: 527}}
          alt="A picture of me and my wife"
        />
        <ImageListItemBar title={'Wonderful Wifey'} />
      </ImageListItem>
      <ImageListItem>
        <img 
          src={headshot}
          style={{ width: 421, height: 527}}
          alt="A picture of me"
        />
        <ImageListItemBar title={'The Man, The Myth, The Legend'} />
      </ImageListItem>
    </Carousel>
  )
}

export default PicturesCarousel