import { Box } from "@mui/material"
import DemosAccordion from './components/demos_accordion'
import PicturesCarousel from './components/pictures_carousel'
import AboutMe from './components/about_me'
import AboutThisSite from "./components/about_this_site"
import QuickInfo from "./components/quick_info"


const Index = () => {
  return (
    // TODO: Consider using container instead of box for flex properties
    <Box>
      <PicturesCarousel />
      <QuickInfo />
      <AboutThisSite />
      <AboutMe />
      <DemosAccordion />
    </Box>
  )
}

export default Index