import { useContext, MouseEvent } from "react"
import { MenuList, Collapse } from "@mui/material"
import MenuElement from "./menu_element"
import { IndexMenuContext } from "../../../contexts/index_menu_context"

interface IndexMenuProps {
  toggleMenu: () => void
}

const IndexMenu = ({ toggleMenu }: IndexMenuProps ) => {
  const {
    expandProfExp,
    setExpandProfExp,
  } = useContext(IndexMenuContext)
   

  const handleMenuClick = (event: MouseEvent) => {
    const target = event?.target as HTMLElement
    const targetParent = target?.parentNode as HTMLElement
    if (
        target?.innerHTML === 'Professional Experience'
        || target?.id === 'expandprofexp' 
        || targetParent?.id === 'expandprofexp'
       ) {
      event?.preventDefault()
      setExpandProfExp(!expandProfExp)
    }
    else{
      toggleMenu()
    }
  }
  return (
    <MenuList component='nav'>
      <MenuElement
        navTarget='#aboutsite'
        menuItemText='About This Site'
        clickHandler={handleMenuClick}
      />
      <MenuElement
        navTarget='#whoami'
        menuItemText='Whoami'
        clickHandler={handleMenuClick}
      />
      <MenuElement
        menuItemText='Professional Experience'
        expand={expandProfExp}
        collapseId='expandprofexp'
        clickHandler={handleMenuClick}
      />
      <Collapse in={expandProfExp}>
        <MenuList>
          <MenuElement
            navTarget='#teksystems'
            menuItemText='TEKSystems'
            clickHandler={handleMenuClick}
            sx={{ pl: 4 }}
          />
          <MenuElement
            navTarget='#briebug'
            menuItemText='BrieBug'
            clickHandler={handleMenuClick}
            sx={{ pl: 4 }}
          />
          <MenuElement
            navTarget='#usaa'
            menuItemText='USAA'
            clickHandler={handleMenuClick}
            sx={{ pl: 4 }}
          />
          <MenuElement
            navTarget='#texastech'
            menuItemText='Texas Tech University'
            clickHandler={handleMenuClick}
            sx={{ pl: 4 }}
          />
          <MenuElement
            navTarget='#internships'
            menuItemText='Internships'
            clickHandler={handleMenuClick}
            sx={{ pl: 4 }}
          />
          <MenuElement
            navTarget='#army'
            menuItemText='US Army'
            clickHandler={handleMenuClick}
            sx={{ pl: 4 }}
          />
        </MenuList>
      </Collapse>
      <MenuElement
        navTarget='#demos'
        menuItemText='Demos'
        clickHandler={handleMenuClick}
      />
    </MenuList>
  )
}

export default IndexMenu