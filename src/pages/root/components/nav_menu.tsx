import { useRef, useState } from 'react'
import { 
  Popper,
  Paper,
  IconButton,
  ClickAwayListener,
} from "@mui/material"
import { Menu as MenuIcon } from '@mui/icons-material'
import IndexMenu from './index_menu'

const BaseMenu = () => {
  const anchorNode = useRef(null)
  const [showMenu, setShowMenu] = useState(false)

  return (
    <>
      <IconButton
        ref={anchorNode}
        size="large"
        edge="start"
        color="inherit"
        aria-label="menu"
        sx={{ mr: 2 }}
        onClick={() => setShowMenu(!showMenu)}
      >
        <MenuIcon />
      </IconButton>
      <Popper 
        open={showMenu}
        anchorEl={anchorNode.current}
        placement='bottom-start'
        modifiers={[
          {
            name: 'offset',
            options: {
              offset: [12, 8]
            }
          }
        ]}
      >
        <Paper>
          <ClickAwayListener onClickAway={() => setShowMenu(false)}>
            {/* Need a div here because functional components don't accept refs */}
            <div>
              <IndexMenu toggleMenu={() => setShowMenu(!showMenu)}/>
            </div>
          </ClickAwayListener>
        </Paper>
      </Popper>
    </>
  )
}

export default BaseMenu