import { useContext } from 'react'
import { 
  AppBar,
  Toolbar,
  Typography,
  Button,
  Box,
} from '@mui/material'
import { NavBarContext } from '../../../contexts/navbar_context'
import BaseMenu from './nav_menu'
import { IndexMenuProvider } from '../../../contexts/index_menu_context'
import { downloadResume } from '../../../common/helpers'

const NavBar = () => {
  const { showLoginButton } = useContext(NavBarContext)
  
  return (
    <>
      <AppBar position="fixed">
        <Toolbar sx={{ width: '1240px', margin: 'auto'}}>
          <IndexMenuProvider>
            <BaseMenu />
          </IndexMenuProvider>
          <Typography variant="h6" component="div" sx={{ flexGrow: 0 }}>
            Frank Conlon 
          </Typography>
          <Box sx={{ flexGrow: 1, marginLeft: '10px' }}>
            <Button color='inherit' component='a' href='#experience'>
              <Typography variant='button'>
               Experience
              </Typography>
            </Button>
            <Button color='inherit' onClick={() => downloadResume()}>
              <Typography variant='button'>
                Resume
              </Typography>
            </Button>
          </Box>
          {showLoginButton && <Button color="inherit">Login</Button> }
        </Toolbar>
      </AppBar>
      {/* This prevents the top of the page from being hidden behind the navbar */}
      <Toolbar />
    </>
    
  )
}

export default NavBar