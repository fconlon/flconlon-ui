import {
  MenuItem,
  ListItemText,
} from '@mui/material'
import {
  ExpandLess,
  ExpandMore,
} from '@mui/icons-material'
import { MouseEvent } from 'react'

interface MenuElementProps {
  navTarget?: string,
  menuItemText: string,
  expand?: boolean,
  collapseId?: string,
  sx?: object,
  clickHandler: (eventish: MouseEvent) => void,
}

const MenuElement = ({ navTarget, menuItemText, expand, collapseId, sx, clickHandler}: MenuElementProps) => {
  return (
    <MenuItem
      sx={sx}
      component='a'
      href={navTarget}
      onClick={(event) => clickHandler(event)}
    >
      <ListItemText primary={menuItemText} />
      { expand !== undefined?
          expand ? 
            <ExpandLess id={collapseId} /> 
            : <ExpandMore id={collapseId} />
          : null
      }
    </MenuItem>
  )
}

export default MenuElement