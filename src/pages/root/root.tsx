import Box from '@mui/material/Box'
import { Outlet } from 'react-router-dom'
import NavBar from './components/navbar'

const Root = () => {
  return (
    <>
      <NavBar />
      <Box sx={{ margin: 'auto', width: '1240px' }}>
        <Outlet />
      </Box>
    </>
  )
}

export default Root
