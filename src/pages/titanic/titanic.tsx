import { useEffect, useCallback } from 'react'
import { Box } from '@mui/material'
import { Coordinate, Animation } from 'aframe'
import { 
  Scene,
  Cursor,
  Cylinder,
  Ocean,
  Text,
  Sky
} from './aframe_elements'
import { get_all_data } from './api_calls'
import { Typography } from '@mui/material'

const HEIGHT_MULTIPLIER = 0.005
const ANIMATE_EVENT = new Event('animate')

interface ClassCounts {
  [passengerclass:string]: number
}

interface DataObject {
  [filter: string] : ClassCounts
}

const Titanic = () => {
  const animateText = useCallback((textEl: HTMLElement, count: number) => {
    const textAnimation = (textEl.getAttribute('animation') as unknown) as Animation
    if (!textAnimation){
      window.requestAnimationFrame(() => animateText(textEl, count))
    }
    else{
      const startPos = (textEl.getAttribute('position') as unknown)  as Coordinate
      const newTextY = count * HEIGHT_MULTIPLIER + 0.25
      textAnimation.from = `${startPos.x} ${startPos.y} ${startPos.z}`
      textAnimation.to = `${startPos.x} ${newTextY} ${startPos.z} dur: 1000`
      textEl.dispatchEvent(ANIMATE_EVENT)
    }
  }, [])

  const getNewCylinderY = (filter: string, passengerclass: string, classCounts: ClassCounts) =>{
    if(filter === 'class' || passengerclass === '3') {
      return classCounts[passengerclass] / 2 * HEIGHT_MULTIPLIER
    }
    else if (passengerclass === '2') { 
      return (classCounts[3]+classCounts[2]/2) * HEIGHT_MULTIPLIER
    }
    else {
      return (classCounts[3]+classCounts[2]+classCounts[1]/2) * HEIGHT_MULTIPLIER
    }
  }

  const animateCylinder = useCallback((
      passengerclass: string, count: number, filter: string, classCounts: ClassCounts
    ) => {
    const cylEl = document.querySelector(`[filter="${filter}"][passengerclass="${passengerclass}"]`)
    const animation = (cylEl.getAttribute('animation') as unknown) as Animation
    if(!animation){
      window.requestAnimationFrame(() => animateCylinder(passengerclass, count, filter, classCounts))
    }
    else{
      const animation__2 = (cylEl.getAttribute('animation__2') as unknown) as Animation
      const currHeight = cylEl.getAttribute('height')
      const startPos = (cylEl.getAttribute('position') as unknown)  as Coordinate
      
      const newCylinderY = getNewCylinderY(filter, passengerclass, classCounts)
      animation.from = `${startPos.x} ${startPos.y} ${startPos.z}`
      animation.to = `${startPos.x} ${newCylinderY} ${startPos.z} dur: 1000`
      animation__2.from = currHeight
      animation__2.to = `${count*HEIGHT_MULTIPLIER} dur:1000`
      cylEl.dispatchEvent(ANIMATE_EVENT)
    }
  }, [])

  const transitionCylinders = useCallback((data: DataObject) => {
    // We use aframe's built in attributes to add animated transitions
    // By using the built in attributes we avoid unnecessary rerenders
    // caused by setting a state variable.
    Object.entries(data).forEach(([filter, classCounts]) =>{
      if (filter === 'class'){
        for (let i = 1; i < 4; i++){
          const textEl = document.querySelector(`[cylinder="${i}"]`)
          animateText(textEl, classCounts[i])
        }
      }
      else {
        const count = classCounts[1] + classCounts[2] + classCounts[3]
        const textEl = document.querySelector(`[cylinder="${filter}"]`)
        animateText(textEl, count)
      }
      Object.entries(classCounts).forEach(([passengerclass, count]) => 
        animateCylinder(passengerclass, count, filter, classCounts)
      )
    })
  }, [animateText, animateCylinder])

  useEffect(() => {
    get_all_data(new Set()).then((data) => transitionCylinders(data))
    
  }, [transitionCylinders])

  const handleClick = (event: MouseEvent) =>{
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    let selected = (event.target! as HTMLElement).getAttribute('selected')
    selected = selected === 'true' ? 'false' : 'true'
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    let filter = (event.target! as HTMLElement).getAttribute('filter')
    if (filter === 'class'){
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (event.target! as HTMLElement).setAttribute('selected',selected)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const pclass = (event.target! as HTMLElement).getAttribute('passengerclass')
      const label = document.querySelector(`[cylinder="${pclass}"]`)
      if (selected === 'true'){
        label.setAttribute('color', 'green')
      }
      else{
        label.setAttribute('color', 'darkblue')
      }
    }
    else{
      const filterCylinders = document.querySelectorAll(`[filter=${filter}]`)
      filterCylinders.forEach((cylinder) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        cylinder.setAttribute('selected', selected!)
      })
      const label = document.querySelector(`[cylinder="${filter}"]`)
      if (selected === 'true'){
        label.setAttribute('color', 'green')
      }
      else{
        label.setAttribute('color', 'darkblue')
      }
    }
    const cylinders = Array.from(document.getElementsByTagName('a-cylinder'))
    const filters = new Set<string>()
    cylinders.forEach((cylinder) => {
      if (cylinder.getAttribute('selected') === 'true'){
        filter = cylinder.getAttribute('filter')
        if (filter === 'class'){
          filter += cylinder.getAttribute('passengerClass')
        }
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        filters.add(filter!)
      }
      
    })
    // // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    get_all_data(filters).then((data) => transitionCylinders(data))
  }

  return (
    <>
      <Typography variant='h6' id='instruction' style={{ 'textAlign': 'center'}}>
        Click a cylinder to apply a filter. <br></br>
        Use WASD to move within the scene. <br></br>
        Click and drag to rotate the camera in the scene.
      </Typography>  
      <Box sx={{height:800}}>
        <Scene>
          <Cursor />
          <Sky />
          <Ocean />
          {/* <Assets />
          <Model /> */}
          
          {/* passenger class display */}
          <Text value='Passenger\nClass' position='-3, 0.1 -3.5'/>
          <Text value='Third' position='-3 0.25 -4' cylinder={3} />
          <Cylinder 
            position={'-3 0 -4'}
            onClick={(event) => handleClick(event)}
            color='maroon'
            filter='class'
            passengerclass={3}
          />
          <Text value='Second' position='-3 0.25 -5' cylinder={2} />
          <Cylinder
            position={'-3 0 -5'}
            onClick={(event) => handleClick(event)}
            color='gray'
            filter='class'
            passengerclass={2}
          />
          <Text value='First' position='-3 0.25 -6' cylinder={1} />
          <Cylinder
            position={'-3 0 -6'}
            onClick={(event) => handleClick(event)}
            color='tan'
            filter='class'
            passengerclass={1}
          />

          {/* gender display */}
          <Text value='Gender' position='-1, 0.1 -3.5'/>
          {/* female */}
          <Text value='Female' position='-1 0.25 -4' cylinder='female' />
          <Cylinder 
            position={'-1 0 -4'}
            onClick={(event) => handleClick(event)}
            color='maroon'
            filter='female'
            passengerclass={3}
          />
          <Cylinder
            position={'-1 0 -4'}
            onClick={(event) => handleClick(event)}
            color='gray'
            filter='female'
            passengerclass={2}
          />
          <Cylinder
            position={'-1 0 -4'}
            onClick={(event) => handleClick(event)}
            color='tan'
            filter='female'
            passengerclass={1}
          />

          {/* male */}
          <Text value='Male' position='-1 0.25 -6' cylinder='male' />
          <Cylinder 
            position={'-1 0 -6'}
            onClick={(event) => handleClick(event)}
            color='maroon'
            filter='male'
            passengerclass={3}
          />
          <Cylinder
            position={'-1 0 -6'}
            onClick={(event) => handleClick(event)}
            color='gray'
            filter='male'
            passengerclass={2}
          />
          <Cylinder
            position={'-1 0 -6'}
            onClick={(event) => handleClick(event)}
            color='tan'
            filter='male'
            passengerclass={1}
          />

          {/* survivor status display */}
          <Text value='Survivor\nStatus' position='1, 0.1 -3.5'/>
          {/* lived */}
          <Text value='Lived' position='1 0.25 -4' cylinder='lived' />
          <Cylinder 
            position={'1 0 -4'}
            onClick={(event) => handleClick(event)}
            color='maroon'
            filter='lived'
            passengerclass={3}
          />
          <Cylinder
            position={'1 0 -4'}
            onClick={(event) => handleClick(event)}
            color='gray'
            filter='lived'
            passengerclass={2}
          />
          <Cylinder
            position={'1 0 -4'}
            onClick={(event) => handleClick(event)}
            color='tan'
            filter='lived'
            passengerclass={1}
          />

          {/* died */}
          <Text value='Died' position='1 0.25 -6' cylinder='died' />
          <Cylinder 
            position={'1 0 -6'}
            onClick={(event) => handleClick(event)}
            color='maroon'
            filter='died'
            passengerclass={3}
          />
          <Cylinder
            position={'1 0 -6'}
            onClick={(event) => handleClick(event)}
            color='gray'
            filter='died'
            passengerclass={2}
          />
          <Cylinder
            position={'1 0 -6'}
            onClick={(event) => handleClick(event)}
            color='tan'
            filter='died'
            passengerclass={1}
          />

          {/* age display */}
          <Text value='Age' position='3, 0.1 -3.5'/>
          {/* child */}
          <Text value='Child' position='3 0.25 -4' cylinder='child' />
          <Cylinder 
            position={'3 0 -4'}
            onClick={(event) => handleClick(event)}
            color='maroon'
            filter='child'
            passengerclass={3}
          />
          <Cylinder
            position={'3 0 -4'}
            onClick={(event) => handleClick(event)}
            color='gray'
            filter='child'
            passengerclass={2}
          />
          <Cylinder
            position={'3 0 -4'}
            onClick={(event) => handleClick(event)}
            color='tan'
            filter='child'
            passengerclass={1}
          />

          {/* adult */}
          <Text value='Adult' position='3 0.25 -6' cylinder='adult' />
          <Cylinder 
            position={'3 0 -6'}
            onClick={(event) => handleClick(event)}
            color='maroon'
            filter='adult'
            passengerclass={3}
          />
          <Cylinder
            position={'3 0 -6'}
            onClick={(event) => handleClick(event)}
            color='gray'
            filter='adult'
            passengerclass={2}
          />
          <Cylinder
            position={'3 0 -6'}
            onClick={(event) => handleClick(event)}
            color='tan'
            filter='adult'
            passengerclass={1}
          />
        </Scene>
      </Box>
      
    </>
  )
}

export default Titanic