import { TITANIC_ENDPOINT } from "../../common/constants"

const get_all_data = async (filters: Set<string>) => {
  const queryString = Array.from(filters).join(',')
  const url = TITANIC_ENDPOINT + '/all_data?filters=' + queryString
  try{
    const response = await fetch(url)
    return response.json()
  }
  catch(error){
    console.error(error)
  }
}

export {get_all_data}