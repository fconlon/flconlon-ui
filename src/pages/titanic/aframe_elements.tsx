import {
  createElement,
  PropsWithChildren,
} from 'react'
import 'aframe'
import 'aframe-extras'

interface CylinderProps {
  position: string,
  onClick: (event: MouseEvent) => void,
  color: string,
  filter: string,
  passengerclass: number,
}

interface TextProps {
  position: string,
  value: string,
  cylinder?: string | number,
}

const Scene = ({ children }: PropsWithChildren) => createElement(
  'a-scene',
  { embedded: true},
  children
)

const Cursor = () => createElement(
  'a-entity',
  { cursor: 'rayOrigin:mouse' }
)

const Cylinder = ({ position, onClick, color='red', filter, passengerclass }: CylinderProps) => createElement(
  'a-cylinder',
  {
    position: position,
    radius: 0.1,
    height: 0,
    color: color,
    onClick: onClick,
    selected: false,
    filter: filter,
    passengerclass: passengerclass,
    animation: `startEvents: animate; property: position; from: ${position}; to: ${position} dur: 1000`,
    animation__2:'startEvents: animate; property: height; from: 1; to: 0.5 dur: 1000',
  }
)

const Plane = () => createElement(
  'a-plane',
  {
    height: 6,
    width: 10,
    color: 'blue',
    rotation: '-90 0 0',
    position: '0 0 -5',
  }
)

const Text = ({ value, position, cylinder='none' }: TextProps) => createElement(
  'a-text',
  {
    cylinder: cylinder,
    value: value,
    position: position,
    color: 'darkblue',
    align: 'center',
    baseline: 'bottom',
    side: 'double',
    animation: `startEvents: animate; property: position; from: ${position}; to: ${position} dur: 1000`,
  }
)

const Ocean = () => createElement(
  'a-ocean',
  {
    position: '0 0 -5',
    width: 100,
    depth: 100,
    density: 100,
    speed: 0.1,
    speedVariance: 2
  }
)

const Assets = () => createElement(
  'a-assets',
  null,
  createElement(
    'a-asset-item',
    {
      id: 'model',
      src: '../../assets/titanic/scene.gltf'
    }
  )
)

const Model = () => createElement(
  'a-gltf-model',
  { 
    src: '#model',
    position: '-1 0 -15',
    scale: '0.12 0.12 0.12',
    rotation: '0 180 0'
  }
)

const Sky = () => createElement(
  'a-sky',
  { color: 'skyblue' }
)

export {
  Scene,
  Cursor,
  Cylinder,
  Plane,
  Text,
  Ocean,
  Assets,
  Model,
  Sky,
}