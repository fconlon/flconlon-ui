import { StrictMode,
  Suspense,
  lazy
} from 'react'
import { createRoot } from 'react-dom/client'
import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
  Route
} from 'react-router-dom'
import Root from './pages/root/root.js'
import Index from './pages/index/index'
import { NavBarProvider } from './contexts/navbar_context'
import './css/index.css'

const ChoreTracker = lazy(() => import('./pages/chore_tracker/chore_tracker'))
const Titanic = lazy(() => import('./pages/titanic/titanic'))

const PlaceHolder = () => {
  return (
    <h1>GO PACK GO!</h1>
  )
}

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path={'/'} element={<Root/>}>
      <Route errorElement={<h1 
        style={{ textAlign: 'center'}}>Oops, Something Went Wrong</h1>}
      >
        <Route index element={<Index />} />
        <Route path={'choretracker'} element={
          <Suspense fallback={<h1>LOADING</h1>}>
            <ChoreTracker />
          </Suspense>
        }/>
        <Route path={'titanic'} element={
          <Suspense fallback={<h1>LOADING</h1>}>
            <Titanic />
          </Suspense>
        }/>
        <Route path={'gitlab'} element={<PlaceHolder />} />
      </Route>
    </Route>
  )
)

createRoot(document.getElementById('root') as HTMLElement).render(
  <NavBarProvider>
    <StrictMode>
      <RouterProvider router={router} />
    </StrictMode>
  </NavBarProvider>
  ,
)
