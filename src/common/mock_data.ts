const response_data = {
  selected: {
    class:{
      1: 2,
      2: 2,
      3: 2,
    },
    female:{
      1: 2,
      2: 2,
      3: 2,
    },
    male:{
      1: 2,
      2: 2,
      3: 2,
    }
    ,
    lived:{
      1: 2,
      2: 2,
      3: 2,
    }
    ,
    died:{
      1: 2,
      2: 2,
      3: 2,
    },
    adult:{
      1: 2,
      2: 2,
      3: 2,
    },
    child:{
      1: 2,
      2: 2,
      3: 2,
    }
  },
  not_selected: {
    class:{
      1: 1,
      2: 1,
      3: 1,
    },
    female:{
      1: 1,
      2: 1,
      3: 1,
    },
    male:{
      1: 1,
      2: 1,
      3: 1,
    }
    ,
    lived:{
      1: 1,
      2: 1,
      3: 1,
    }
    ,
    died:{
      1: 1,
      2: 1,
      3: 1,
    },
    adult:{
      1: 1,
      2: 1,
      3: 1,
    },
    child:{
      1: 1,
      2: 1,
      3: 1,
    }
  }
}

export { response_data }