import resume from '../assets/Frank-Conlon.pdf'

const downloadResume = () => {
  // have to cast as unknown and recast to HTMLAnchorElement due to 
  // conflict with aframe 'a' element
  const link = <unknown>document.createElement('a') as HTMLAnchorElement
  link.href = resume
  link.download = 'Frank Conlon Resume.pdf'
  link.click()
}

export {
  downloadResume
}