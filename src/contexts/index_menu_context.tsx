import {
  createContext,
  useState,
  PropsWithChildren,
  Dispatch,
  SetStateAction,
} from "react"

type noop = () => void

interface IndexMenuContextType {
  expandProfExp: boolean,
  setExpandProfExp: noop | Dispatch<SetStateAction<boolean>>,
}

const default_context = {
  expandAboutMe: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setExpandAboutMe: () => {},
  expandProfExp: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setExpandProfExp: () => {},
}

const IndexMenuContext = createContext<IndexMenuContextType>(default_context)

const IndexMenuProvider = ({ children }: PropsWithChildren) => {
  const [expandProfExp, setExpandProfExp] = useState(false)

  const exportedProps = {
    expandProfExp,
    setExpandProfExp,
  }

  return (
    <IndexMenuContext.Provider  value={exportedProps}>
      {children}
    </IndexMenuContext.Provider>
  )
}

export { IndexMenuContext, IndexMenuProvider }