import {
  createContext,
  useState,
  PropsWithChildren,
  Dispatch,
  SetStateAction
} from "react"

type noop = () => void

interface NavBarContextType {
  showLoginButton: boolean,
  setShowLoginButton: noop | Dispatch<SetStateAction<boolean>>,
}

const default_context = {
  showLoginButton: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setShowLoginButton: () => {},
}

const NavBarContext = createContext<NavBarContextType>(default_context)

const NavBarProvider = ({ children }: PropsWithChildren) => {
  const [showLoginButton, setShowLoginButton] = useState<boolean>(false)

  return (
    <NavBarContext.Provider  value={{ showLoginButton, setShowLoginButton }}>
      {children}
    </NavBarContext.Provider>
  )
}

export { NavBarContext, NavBarProvider }